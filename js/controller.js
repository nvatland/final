app.controller('indexController', function ($scope, $http, $location, $routeParams) {
	$scope.hello = 'hello';
});

app.controller('homeCtrl', function ($scope, $http, $location, $routeParams) {
	$scope.hello = 'hello HOME PAGE';
});

app.controller('projectsCtrl', function ($scope, $http, $location, $routeParams) {
	$scope.hello = 'hello PROJECTS PAGE';
	$http.get('http://nicolevatland.com/angular/final/js/projects.json').then(function(response){
		console.log(response.data);
		$scope.projects = response.data;
	});
});

app.controller('contactCtrl', function ($scope, $http, $location, $routeParams) {
	$scope.success = false;
    $scope.error = false;

    $scope.sendMessage = function( contact ) {
      contact.submit = true;
      $http({
          method: 'POST',
          url: 'processForm.php',
          data: angular.element.param(contact),
          headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
      })
      .success( function(data) {
        if ( data.success ) {
          $scope.success = true;
        } else {
          $scope.error = true;
        }
      } );
    }
});

app.controller('singleCtrl', function ($scope, $http, $location, $routeParams){
	var controller = this;
	console.log($routeParams.id);
	$http.get('http://nicolevatland.com/angular/final/js/projects.json').success(function(data){
        angular.forEach(data, function(item) {
          if (item.id == $routeParams.id) {
            $scope.project = item;
        	console.log($scope.project);
        }
    });
    });
});