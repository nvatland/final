var app = angular.module('finalProject', ['ngRoute']);

app.config(function($routeProvider, $locationProvider){
    $routeProvider
     .when('/', {
           templateUrl : '/angular/final/templates/index/index.html',
           controller: 'homeCtrl'
      })
      .when('/contact', {
           templateUrl: '/angular/final/templates/contact/index.html',
           controller: 'contactCtrl'
      })
      .when('/projects', {
        templateUrl: '/angular/final/templates/projects/index.html',
        controller: 'projectsCtrl'
      })
      .when('/projects/:id', {
        templateUrl: '/angular/final/templates/single/index.html',
        controller: 'singleCtrl'
      })
      .otherwise({redirectTo: '/'});
});